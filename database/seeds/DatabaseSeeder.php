<?php

use App\User;
use App\Comment;
use App\Profile;
use App\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->truncate();
        DB::table('profiles')->truncate();
        DB::table('posts')->truncate();
        DB::table('comments')->truncate();



        $user = new User;
        $user->name="Frank";
        $user->lastname="Sinatra";
        $user->username="frank.s";
        $user->password= bcrypt('iamthebest');
        $user->email="f.sinatra@myway.com";
        $user->save();

        $profile = new Profile;
        $profile->image="../img/frank1.jpg";
        $profile->music="Ella Fitzgerald, Judy Garland";
        $profile->film="till the clouds roll by, From here to eternity, High society, 4 for Texas";
        $profile->about="I am a man passionate about singing, I do things my way in New York yes New York.
        I hope one day someone can fly me to the moon to do somethin' stupid and say it was a very good year. night and day.
        espacially when I meet the girl from ipanema";
        $profile->user_id = $user->id;
        $profile->save();

        $post = new Post;
        $post->image="../img/sinatra.jpeg";
        $post->content="Back in the studio, how I love it!";
        $post->user_id = $user->id;
        $post->save();

        $user = new User;
        $user->name="Madelin";
        $user->lastname="Mcshamrock";
        $user->username="MadShamrock";
        $user->password= bcrypt('ireland');
        $user->email="madelin.mcshamrock@email.ie";
        $user->save();

        $profile = new Profile;
        $profile->image="../img/madelin1.jpg";
        $profile->music="The Cranberries, U2, The Dubliners";
        $profile->film="The guard, The Secret of Kells, Shadow Dancer";
        $profile->about="I am an irish woman, born and raised in Cork.
        My hobby's are darts and swimming.
        I like everything irish from music to films.
        I fancy a good cider with chips in curry sauce. ";
        $profile->user_id = $user->id;
        $profile->save();

        $post = new Post;
        $post->image="../img/ireland.jpg";
        $post->content="Roadtrip with my mates! How I love our country!";
        $post->user_id = $user->id;
        $post->save();

        $comment = new Comment;
        $comment->text="Dit was goede muziek";
        $comment->post_id = $post->id;
        $comment->user_id = $user->id;
        $comment->save();

        $user = new User;
        $user->name="Robin";
        $user->lastname="Beckers";
        $user->username="Robers";
        $user->password= bcrypt('robers');
        $user->email="robinbeckers@gmail.com";
        $user->save();

        $profile = new Profile;
        $profile->image="../img/robin1.jpeg";
        $profile->music="schlagers";
        $profile->film="John Wick";
        $profile->about="I am currently working as a freelance marketing director and i’m always interested in a challenge. Reach out to robinbeckers@gmail.com to connect!";
        $profile->user_id = $user->id;
        $profile->save();

        $post = new Post;
        $post->image="../img/schlager.jpg";
        $post->content="Schlager festival Belgium!!!";
        $post->user_id = $user->id;
        $post->save();

        $comment = new Comment;
        $comment->text="Daar was volop ambiance";
        $comment->post_id = $post->id;
        $comment->user_id = $user->id;
        $comment->save();

        $comment = new Comment;
        $comment->text="Volgende maal ga ik ook mee";
        $comment->post_id = $post->id;
        $comment->user_id = $user->id;
        $comment->save();

        $user = new User;
        $user->name="Dennis";
        $user->lastname="Slenders";
        $user->username="Denders";
        $user->password= bcrypt('denders');
        $user->email="dennisslenders@gmail.com";
        $user->save();

        $profile = new Profile;
        $profile->image="../img/dennis1.jpeg";
        $profile->music="metal";
        $profile->film="The pick of destiny";
        $profile->about="Feisty, stubborn, sarcastic, outspoken, very, very blunt - Hmm, are these my good points or bad? Who knows! Who cares! It's still gonna be me";
        $profile->user_id = $user->id;
        $profile->save();

        $post = new Post;
        $post->image="../img/gmm.jpg";
        $post->content="Graspop here we come! VOLBEAT VOLBEAT VOLBEAT";
        $post->user_id = $user->id;
        $post->save();

        $comment = new Comment;
        $comment->text="Dat is een super festival";
        $comment->post_id = $post->id;
        $comment->user_id = $user->id;
        $comment->save();




    }

}

@extends('layouts.frontendprofile')

@section('content')

<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">Profile </h4>
                    <p class="text-muted">Muziek: {{ $user->profile->music }}<br>Films: {{ $user->profile->film }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <a href="#" class="navbar-brand d-flex align-items-center">
                <div class="title">Laratalk</div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>

<main role="main">

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">{{ $user->name }} {{ $user->lastname }}</h1>
            <h2>{{ $user->username }}</h2>
            <img src="{{ $user->profile->image  }}" width="180" height="200">
            <p class="lead text-muted">{{ $user->profile->about }}</p>
            <p>
                @if(Auth::check()&& $user->id == Auth::id())
                    <a href="{{ route('profile.edit', Auth::id()) }}" class="btn btn-primary my-2">Edit profile</a>
                @else
                    <a href="#" class="btn btn-secondary my-2">Follow</a>
                @endif
                <a href="{{ route('profile', Auth::id()) }}" class="btn btn-secondary my-2">Back</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <h4>{{ $user->username }}</h4>
                        <a href="{{ route('post.add') }}"><img class="card-img-top"
                                                               src="{{ URL::asset('img/img_placeholder.jpg') }}"
                                                               class="postpic" width="250" height="250"
                                                               alt="Card image cap"></a>
                        <div class="card-body">
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet
                                neque a est accumsan iaculis. Duis eu tellus ac urna varius tincidunt. Nunc sollicitudin
                                odio vitae mi facilisis tempus.</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ route('post.add') }}">
                                        <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                    </a>
                                    <a href="#">
                                        <button type="button" class="btn btn-sm btn-outline-secondary">Like</button>
                                    </a>
                                </div>
                                <small class="text-muted">posted on</small>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($user->posts as $post)
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <h4>{{ $user->username }}</h4>
                            <a href="{{ route('post.show', $user->id) }}"><img class="card-img-top"
                                                                               src="{{ $post->image }}" class="postpic"
                                                                               width="250" height="250"
                                                                               alt="Card image cap"></a>
                            <div class="card-body">
                                <p class="card-text">{{ $post->content }}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="{{ route('post.show', $user->id) }}">
                                            <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                        </a>
                                        <a href="#">
                                            <button type="button" class="btn btn-sm btn-outline-secondary">Like</button>
                                        </a>
                                    </div>
                                    <small class="text-muted">Posted on</small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
    </div>
</footer>

@endsection
@extends('layouts.frontendview')

@section('content')

    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <a class="navbar-brand" href="#">
        <div class="title">Laratalk</div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home" title="Home"></i></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('profile') }}"><i class="far fa-user-circle" title="Meet People"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fas fa-calendar-alt" title="Note Feed"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('post') }}"><i class="fas fa-newspaper" title="News Feed"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('post.add', Auth::id()) }}"><i class="fas fa-comments"
                                                                                  title="Let's Talk"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile.show', Auth::id()) }}"><i class="fas fa-user"
                                                                                      title="My profile"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt" title="Bye Bye"></i></a>
            </li>
        </ul>
    </div>
</nav>


<main role="main" class="container">
    <div class="jumbotron">
        <h1>Discover People</h1>
        <hr>
        @foreach($users as $user)
            <a class="profilelink" href="{{ route('profile.show', $user->profile->id) }}"><h3>{{ $user->username }}</h3>
            </a>
            <a class="profilelink" href="#"><img src="{{ $user->profile->image }}" class="profilethumb"></a>
            <a class="btn btn-lg btn-primary float-right" href="#" role="button">Follow</a><br/>
            @foreach($user->posts as $post)
                <a href="{{ route('post.show', $user->id) }}"><img src="{{ $post->image }}" class="postpic" width="300"
                                                                   height="300"></a>
            @endforeach
            <hr>
        @endforeach
    </div>

</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
    </div>
</footer>

@endsection



@extends('layouts.frontend')

@section('content')
    <div class="huls">
        <h3 class="laratalk">"Edit your profile to let people know how special you are"</h3>

        <div class="form-group">
            <h1 class="title2">Laratalk</h1>
            <h3>Profile</h3>
        </div>
        <form action="{{ route('profile.update', $user->id )}}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Image</label>
                <input type="text" class="form-control" name="image" value="{{ $user->profile->image }}" id="image"
                       aria-describedby="nameHelp" placeholder="../img/default-user.png or .jpg">
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}" id="name"
                       aria-describedby="nameHelp" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="lastname">Lastname</label>
                <input type="text" class="form-control" name="lastname" value="{{ $user->lastname }}" id="lastname"
                       aria-describedby="lastnameHelp" placeholder="Enter lastname">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" value="{{ $user->username }}" id="username"
                       aria-describedby="usernameHelp" placeholder="Enter username">
            </div>
            <div class="form-group">
                <label for="Email">Email address</label>
                <input type="email" class="form-control" name="email" value="{{ $user->email }}" id="Email1"
                       aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
                </small>
            </div>
            <div class="form-group">
                <label for="muziek">Favorite muziek</label>
                <input type="text" class="form-control" name="music" value="{{ $user->profile->music }}" id="muziek"
                       placeholder="enter favoriete muziek">
            </div>
            <div class="form-group">
                <label for="films">Favorite films</label>
                <input type="text" class="form-control" id="films" value="{{ $user->profile->film }}" name="film"
                       placeholder="enter favorite films">
            </div>
            <div class="form-group">
                <label for="about">About you</label>
                <textarea type="about" class="form-control" name="about" value="{{ $user->profile->about }}" id="about"
                          placeholder="enter about you"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Show how special I am!</button>
            </div>
        </form>
    </div>
@endsection
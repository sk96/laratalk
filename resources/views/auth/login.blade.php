@extends('layouts.frontend')

@section('content')
    <div class="huls">
<h3 class="laratalk">"Enter your own world, get ready for your following journey"</h3>

    <form action="{{ route('login.post') }}" method="post">
        @include('shared.errors')
        {{ csrf_field() }}
    <div class="form-group">
        <h1 class="title2">Laratalk</h1>
        <h3>Log in</h3>
    </div>

        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" id="username" aria-describedby="usernameHelp" placeholder="Enter username">
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="usernameHelp" placeholder="Enter email adress">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password"  id="password" placeholder="Password">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Continue my journey!</button>
        </div>
    </form>
</div>
@endsection
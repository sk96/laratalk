@extends('layouts.frontend')

@section('content')
    <div class="huls">
        <h3 class="laratalk">"Register quick. Because everyone loves a good talk"</h3>

        <div class="form-group">
            <h1 class="title2">Laratalk</h1>
            <h3>Register</h3>
        </div>
        <form action="{{ route('register.post') }}" method="post">
            @include('shared.errors')
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="lastname">Lastname</label>
                <input type="text" class="form-control" name="lastname" id="lastname" aria-describedby="lastnameHelp"
                       placeholder="Enter lastname">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control"  name="username" id="username" aria-describedby="usernameHelp"
                       placeholder="Enter username">
            </div>
            <div class="form-group">
                <label for="Email">Email address</label>
                <input type="email" class="form-control" name="email" id="Email1" aria-describedby="emailHelp"
                       placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
                </small>
            </div>
            <div class="form-group">
                <label for="Password">Password</label>
                <input type="password" class="form-control" name="password" id="Password" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="Password">Confirm password</label>
                <input type="password" class="form-control" name="password_confirmation" id="Password_confirmation" placeholder="Confirm password">
            </div>
            <div class="form-group">
                <button type="submit"  class="btn btn-primary">Start my journey!</button>
            </div>
        </form>
    </div>
@endsection
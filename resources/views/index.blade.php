@extends('layouts.frontendindex')

@section('content')

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Laratalk</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link active" href="#">Home</a>
            @if(Auth::check())
            <a class="nav-link" href="{{ route('logout') }}">Logout</a>
              <a class="nav-link" href="{{ route('profile') }}">Meet People</a>
            @else
            <a class="nav-link" href="{{ route('login') }}">Login</a>
            <a class="nav-link" href="{{ route('register') }}">Sign up</a>
            @endif

          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
        <h1 class="cover-heading">Welcome</h1>
        <p class="title">Enter the world of laratalk. create your very own journey, follow other minds and share yours with many of our users..</p>
        <p class="lead">
          @if(!Auth::check())
          <a href="{{ route('login') }}" class="btn btn-lg btn-secondary">Login</a>
          <a href="{{ route('register') }}" class="btn btn-lg btn-secondary">Sign up</a>
            @endif
        </p>
      </main>

       <img src="{{ URL::asset('img/collage.jpg') }}">

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p><a href="#">Eindwerk PHP Framework, by Suzanne, Sven and Pieter</a>.</p>
        </div>
      </footer>
    </div>

@endsection


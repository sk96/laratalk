@extends('layouts.frontendview')

@section('content')

<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <a class="navbar-brand" href="#">
        <div class="title">Laratalk</div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home" title="Home"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile') }}"><i class="far fa-user-circle" title="Meet People"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fas fa-calendar-alt" title="Note Feed"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('post') }}"><i class="fas fa-newspaper" title="News Feed"></i></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('post.add') }}"><i class="fas fa-comments"
                                                                      title="Let's Talk"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile.show', Auth::id()) }}"><i class="fas fa-user"
                                                                                      title="My profile"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt" title="Bye Bye"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col"></div>
        <div class="col-md-6 jumbotron">
            <small class="text-muted">Posted on :</small>
            <hr>
            <img class="card-img-top align-content-center" width="350" height="350"
                 src="{{ URL::asset('img/img_placeholder.jpg') }}" alt="Card image cap">
            <div class="card-body">
                <form action="{{ route('add.post', Auth::id() )}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">

                        <input type="text" class="form-control" id="image" value="" name="image"
                               placeholder="Share here your photo">
                    </div>
                    <div class="form-group">

                        <textarea type="about" rows="6" class="form-control" name="content" value="" id="about"
                                  placeholder="Tell here what you wanna share with us...."></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Tell your story!</button>
                        <a href="{{ route('profile') }}" class="btn btn-secondary my-2">Back</a>
                    </div>
                </form>

            </div>
        </div>
        <div class="col"></div>
    </div>
</div>

@endsection


@extends('layouts.frontendprofile')

@section('content')

<main role="main">

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">{{ $user->name }} {{ $user->lastname }}</h1>
            <h2>{{ $user->username }}</h2>
            <img src="{{ $user->profile->image  }}" width="150" height="200">
            <p class="lead text-muted">{{ $user->profile->about }}</p>
            <p>
                <a href="{{ route('profile', Auth::id()) }}" class="btn btn-secondary my-2">Back</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">

        <div class="container">

            <div class="row">

                    <div class="col-md-6">
                        @foreach($user->posts as $post)
                        <h1>My post</h1>
                        <img src="{{ $post->image }}" width="500" height="400" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">{{ $post->content }}</p>
                            @endforeach
                            <div class="d-flex justify-content-between align-items-center">
                                <form class="float-left" action="{{ route('comment.post', $post->id )}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">

                                        <input type="text" class="form-control" id="text" value="" name="text"
                                               placeholder="Tell us something...">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Give your comment!</button>
                                        <p class="text-muted">Or like the post !! <a href="#"><i
                                                        class="far fa-thumbs-up"></i></a></p>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>

            </div>
            <aside>
                <h1>Comments</h1>
                <hr>
                @foreach($user->comments()->orderBy('id','desc')->get() as $comment)
                   <article>
                    <p>{{ $comment->text }}</p>
                       <hr>
                   </article>
                @endforeach
            </aside>
        </div>
    </div>

</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
    </div>
</footer>

@endsection


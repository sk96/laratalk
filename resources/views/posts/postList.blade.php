@extends('layouts.frontendview')

@section('content')

<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <a class="navbar-brand" href="#">
        <div class="title">Laratalk</div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home" title="Home"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile') }}"><i class="far fa-user-circle" title="Meet People"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fas fa-calendar-alt" title="Note Feed"></i></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('post') }}"><i class="fas fa-newspaper" title="News Feed"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('post.add') }}"><i class="fas fa-comments"
                                                                      title="Let's Talk"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile.show', Auth::id()) }}"><i class="fas fa-user"
                                                                                      title="My profile"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt" title="Bye Bye"></i></a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">
    <div class="jumbotron">
        <h1>News feed</h1>
        <hr>
        @foreach($users as $user)
        <h4>Post of : {{ $user->username }}</h4>
        <div class="card mb-3">
            @foreach($user->posts as $post)
            <div class="card-block">
                <img src="{{ $post->image }}" alt="Card image cap" width="500" height="400" >
                <p class="card-text">{{ $post->content }}</p>
                <hr>
                @endforeach
                <aside>
                    <h1>Comments</h1>
                    @foreach($user->comments()->orderBy('id','desc')->get() as $comment)
                        <article>
                            <p>{{ $comment->text }}</p>
                            <hr>
                        </article>
                    @endforeach
                </aside>
                <hr>
                <div class="d-flex justify-content-between align-items-center">
                    <form class="float-left" action="{{ route('comment.post', $post->id )}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <textarea type="text" class="form-control" id="text" value="" name="text"
                                      placeholder="Tell us something..."></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Give your comment!</button>
                            <p class="text-muted">Or like the post !! <a href="#"><i
                                            class="far fa-thumbs-up"></i></a></p>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
            <hr>
            @endforeach
    </div>
</main>
<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
    </div>
</footer>

@endsection
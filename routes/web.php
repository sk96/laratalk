<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'pageController@index')->name('home');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.post');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register.post');

Route::get('/logout', 'Auth\loginController@logout')->name('logout');

Route::get('/{id}/edit', 'ProfileController@edit')->name('profile.edit');
Route::post('/{id}/update', 'ProfileController@update')->name('profile.update');

Route::get('/{id}/profile', 'ProfileController@show')->name('profile.show');
Route::get('/profile', 'ProfileController@allProfile')->name('profile');

Route::get('/{id}/post', 'PostController@show')->name('post.show');
Route::get('/post', 'PostController@allPosts')->name('post');
Route::post('/{id}/post', 'PostController@postComment')->name('comment.post');

Route::get('/add', 'PostController@add')->name('post.add');
Route::post('/{id}/profile', 'PostController@save')->name('add.post');



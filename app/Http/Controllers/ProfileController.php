<?php

namespace App\Http\Controllers;
use App\Http\Requests\EditProfileRequest;
use App\Profile;
use App\User;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProfileController extends Controller
{
    public function allProfile()
    {
        $users = User::all();
        return view('profile/profileUsers', compact('users'));
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('profile/profiel', compact('user'));

    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('profile/edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $profile = Profile::find($user->profile->id);
        $profile->image = $request->input('image');
        //$profile->image = $request->file('image')->store('public/img');
        $user->name = $request->input('name');
        $user->lastname = $request->input('lastname');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $profile->music = $request->input('music');
        $profile->film = $request->input('film');
        $profile->about = $request->input('about');
        $user->save();
        $profile->save();
        return view('profile/profiel', compact('user'));

    }
}

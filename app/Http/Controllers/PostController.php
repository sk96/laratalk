<?php

namespace App\Http\Controllers;
use App\User;
use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function allPosts()
    {
        $users = User::all();
        return view('posts/postList', compact('users'));
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('posts/post', compact('user'));
    }

    public function add()
    {
        return view('posts/postAdd', compact('user'));
    }
    public function save($id,Request $request)
    {
        $user = User::find($id);
        $post = new Post;
        $post->image = $request->input('image');
        $post->content = $request->input('content');
        $post->user_id = Auth::id();
        //$user->save();
        $post->save();

       // return redirect()->route('profile/ Auth::id()');

        return view('profile/profiel', compact('user'));
    }

    public function postComment($id, Request $request)
    {

        $comment = new Comment;
        $comment->text = $request->input('text');
        $comment->post_id = $id;
        $comment->user_id = $id;
        $comment->save();
        return Redirect()->back();

    }

}
